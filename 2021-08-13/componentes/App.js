import React, { useState } from 'react';
import { Alert, Button, StyleSheet, Text, TextInput, View } from 'react-native';

export default function App() {
  const [temperatura, setTemperatura] = useState('')
  const [resultado, setResultado] = useState('')
  
  const convertirAC = () => {
    let temp = (temperatura - 32) / 1.8
    setResultado(temp + ' °C')
  }
  const convertirAF = () => {
    let temp = (temperatura  * 1.8) + 32
    setResultado(temp + ' °F')
  }

  return (
    <View style={styles.container}>
      <TextInput 
        value={temperatura}
        onChangeText={setTemperatura}
        placeholder="Ingrese una temperatura"
        keyboardType='numeric'
      />
      <Button title='Convertir a °C' onPress={convertirAC} />
      <Button title='Convertir a °F' onPress={convertirAF} />
      <Text>Resultado: {resultado}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
