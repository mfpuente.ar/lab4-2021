import React, { useState } from "react";
import axios from "axios";
import { Button, Text, TextInput, View } from "react-native";
import { useMutation } from "react-query";

export function NuevaTarea({ onNuevaTarea }) {
  const [nombreTarea, setNombreTarea] = useState("");
  const { isLoading, mutate } = useMutation(
    (nuevaTarea) => axios.post("http://10.0.2.2:4000/tareas", nuevaTarea),
    {
      onSuccess: () => {
        setNombreTarea("");
        onNuevaTarea();
      },
    }
  );

  return (
    <View style={{ flexDirection: "row" }}>
      {isLoading ? (
        <Text>Agregando tarea...</Text>
      ) : (
        <>
          <TextInput
            value={nombreTarea}
            onChangeText={setNombreTarea}
            placeholder="Nueva tarea"
          />
          <Button
            title="Agregar"
            onPress={() => mutate({ nombre: nombreTarea, lista: false })}
          />
        </>
      )}
    </View>
  );
}
