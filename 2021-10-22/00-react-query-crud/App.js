import React from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { Tareas } from "./Tareas";

const queryClient = new QueryClient();

export default function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Tareas />
    </QueryClientProvider>
  );
}
