import React from "react";
import { Button, Text, View } from "react-native";

export function Tarea({ tarea }) {
  return (
    <View style={{ flexDirection: "row" }}>
      <Text style={{ color: tarea.lista ? "red" : "black" }}>
        {tarea.nombre}
      </Text>
      <Button title="Completar" />
      <Button title="Eliminar" />
    </View>
  );
}
