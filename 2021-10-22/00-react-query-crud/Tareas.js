import React from "react";
import { FlatList, StyleSheet, Text, View } from "react-native";
import Constants from "expo-constants";
import { useQuery } from "react-query";
import axios from "axios";
import { NuevaTarea } from "./NuevaTarea";
import { Tarea } from "./Tarea";

export function Tareas() {
  const { isFetching, data, isError, error, refetch } = useQuery("tareas", () =>
    axios.get("http://10.0.2.2:4000/tareas")
  );

  if (isError) {
    return (
      <View>
        <Text>{error.messsage}</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      {isFetching ? (
        <Text>Cargando tareas...</Text>
      ) : (
        <>
          <NuevaTarea onNuevaTarea={refetch} />
          <FlatList
            data={data.data}
            renderItem={({ item }) => <Tarea tarea={item} />}
          />
        </>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: Constants.statusBarHeight,
  },
});
