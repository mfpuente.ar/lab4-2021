import React, { createContext, useContext, useState } from "react";

const CuentaContext = createContext();
const SetCuentaContext = createContext();

export function useCuentaContext() {
  return [useContext(CuentaContext), useContext(SetCuentaContext)];
}

export function CuentaProvider({ children }) {
  const [cuenta, setCuenta] = useState(0);

  return (
    <CuentaContext.Provider value={cuenta}>
      <SetCuentaContext.Provider value={setCuenta}>
        {children}
      </SetCuentaContext.Provider>
    </CuentaContext.Provider>
  );
}
