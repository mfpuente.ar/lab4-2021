import React from "react";
import { Button, StyleSheet, Text, useColorScheme, View } from "react-native";
import { CuentaProvider, useCuentaContext } from "./CuentaContext";

function Cuenta() {
  const [cuenta, setCuenta] = useCuentaContext();
  return (
    <View style={{ flexDirection: "row" }}>
      <Button title="-" onPress={() => setCuenta((c) => c - 1)} />
      <Text>{cuenta}</Text>
      <Button title="+" onPress={() => setCuenta((c) => c + 1)} />
    </View>
  );
}

function Cuenta2() {
  const [cuenta, setCuenta] = useCuentaContext();
  return (
    <View style={{ flexDirection: "row" }}>
      <Button title="-" onPress={() => setCuenta((c) => c - 2)} />
      <Text>{cuenta}</Text>
      <Button title="+" onPress={() => setCuenta((c) => c + 2)} />
    </View>
  );
}

export default function App() {
  return (
    <CuentaProvider>
      <View style={styles.container}>
        <Cuenta />
        <Cuenta2 />
      </View>
    </CuentaProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
