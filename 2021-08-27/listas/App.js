import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { FlatList, SafeAreaView, SectionList, StyleSheet, Text, View } from 'react-native';
import Constants from 'expo-constants'

let usuarios = [
  { id: 0, nombre:'Jose', apellido:'Sanchez', edad:30, email:''},
  { id: 1, nombre:'Pedro', apellido:'Sanchez', edad:30, email:''},
  { id: 2, nombre:'Manuel', apellido:'Sanchez', edad:30, email:''},
  { id: 3, nombre:'Juan', apellido:'Sanchez', edad:30, email:''},
]

export default function App() {
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={usuarios}
        renderItem={({ item }) => <Text style={styles.item}>{item.nombre}</Text>}
        keyExtractor={(item) => item.id.toString()}
      />
    </SafeAreaView>
    /*
    <SafeAreaView style={styles.container}>
      <SectionList
        sections={[
          { title: 'A', data: ['Andres', 'Alejandro', 'Abel'] },
          { title: 'C', data: ['Carlos', 'Camilo'] }
        ]}
        renderItem={({ item }) => <Text style={styles.item}>{item}</Text>}
        renderSectionHeader={({ section }) => <Text style={styles.header}>{section.title}</Text>}
        keyExtractor={(item, index) => index}
      />
    </SafeAreaView>
    */
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: Constants.statusBarHeight
  },
  header: {
    fontSize: 14,
    fontWeight: 'bold'
  },
  item: {
    height: 40,
    fontSize: 16,
    padding: 10
  }
});
