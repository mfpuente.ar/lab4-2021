import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialCommunityIcons } from '@expo/vector-icons'

function PantallaInicial({ navigation }) {
  return (
    <View style={styles.container}>
      <Text>Pantalla inicial</Text>
      <MaterialCommunityIcons.Button name="account" size={32} iconStyle={{marginRight:0}}  onPress={() => navigation.navigate('Perfil')} />
    </View>
  )
}

function PantallaPerfil() {
  return (
    <View style={styles.container}>
      <Text>Pantalla de perfil</Text>
    </View>
  )
}

const Tab = createBottomTabNavigator()

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator initialRouteName="Inicial">
        <Tab.Screen name='Inicial' component={PantallaInicial} options={
          {
            tabBarLabel: 'Principal',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="home" color={color} size={size} />
            )
          }} />
        <Tab.Screen name='Perfil' component={PantallaPerfil} options={
          {
            tabBarLabel: 'Usuario',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="account" color={color} size={size} />
            )
          }} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
