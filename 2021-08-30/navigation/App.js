import React, { useEffect, useState } from 'react'
import { Button, Text, View } from 'react-native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { NavigationContainer } from '@react-navigation/native'

function PantallaInicial({ navigation }) {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Pantalla inicial</Text>
      <Button title="Ir a perfil" onPress={() => navigation.navigate('Perfil', { nombre: 'Matias' })} />
    </View>
  )
}

function PantallaPerfil({ navigation, route }) {
  const [cuenta, setCuenta] = useState(0)
  const { nombre } = route.params

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button title="Contar" onPress={() => setCuenta(c => c + 1)} />
      )
    })
  }, [navigation])

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Pantalla perfil</Text>
      <Text>Nombre: {nombre}</Text>
      <Text>Cuenta: {cuenta}</Text>
      <Button title="Volver" onPress={() => navigation.goBack()} />
      <Button title="Cambiar titulo" onPress={() => navigation.setOptions({ title: 'Mi perfil' })} />
    </View>
  )
}

const Stack = createNativeStackNavigator()

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Principal">
        <Stack.Screen name="Principal" component={PantallaInicial} options={{
          title: "Inicial",
          headerShown: false
          }} />
        <Stack.Screen name="Perfil" component={PantallaPerfil} options={{ title: "Perfil" }} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
