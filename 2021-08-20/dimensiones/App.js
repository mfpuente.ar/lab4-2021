import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SafeAreaView, StyleSheet, Text, View } from 'react-native';
import { useDeviceOrientation} from '@react-native-community/hooks'

export default function App() {
  const {portrait, landscape} = useDeviceOrientation()
  return (
    <SafeAreaView style={styles.container}>
      <View style={{
        backgroundColor:'blue',
        width: '100%',
        height: portrait ? '30%': '100%'
      }} />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
