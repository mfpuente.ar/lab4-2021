import React from "react";
import { StyleSheet, Text, View } from "react-native";
import axios from "axios";
import { QueryClient, QueryClientProvider, useQuery } from "react-query";

const queryClient = new QueryClient();

function Usuarios() {
  const { isLoading, data, isError, error } = useQuery("usuarios", () => {
    return axios.get("http://10.0.2.2:4000/usuarios");
  });

  if (isLoading) {
    return (
      <View>
        <Text>Cargando...</Text>
      </View>
    );
  }

  if (isError) {
    return (
      <View>
        <Text>{error.messsage}</Text>
      </View>
    );
  }

  return (
    <View>
      {data.data.map((usuario) => (
        <Text>{usuario.nombre}</Text>
      ))}
    </View>
  );
}

export default function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <View style={styles.container}>
        <Text>Usuarios</Text>
        <Usuarios />
      </View>
    </QueryClientProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
