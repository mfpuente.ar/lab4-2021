import React, { useReducer, useState } from "react";
import {
  Button,
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  View,
} from "react-native";
import Constants from "expo-constants";

function tareaReducer(tareas, action) {
  switch (action.tipo) {
    case "Agregar":
      return [
        ...tareas,
        { id: Date.now(), nombre: action.nombre, lista: false },
      ];
    case "completar":
      return tareas.map((tarea) => {
        if (tarea.id === action.id) {
          return { ...tarea, lista: true };
        }
        return tarea;
      });
    case "eliminar":
      return tareas.filter((tarea) => {
        return tarea.id !== action.id;
      });
  }
}

function Tarea({ tarea, tareaDispatch }) {
  return (
    <View style={{ flexDirection: "row" }}>
      <Text style={{ color: tarea.lista ? "red" : "black" }}>
        {tarea.nombre}
      </Text>
      <Button
        title="Completar"
        onPress={() => tareaDispatch({ tipo: "completar", id: tarea.id })}
      />
      <Button
        title="Eliminar"
        onPress={() => tareaDispatch({ tipo: "eliminar", id: tarea.id })}
      />
    </View>
  );
}

export default function App() {
  const [tareas, tareaDispatch] = useReducer(tareaReducer, []);
  const [nuevaTarea, setNuevaTarea] = useState("");

  return (
    <View style={styles.container}>
      <View style={{ flexDirection: "row" }}>
        <TextInput
          value={nuevaTarea}
          onChangeText={setNuevaTarea}
          placeholder="Nueva tarea"
        />
        <Button
          title="Agregar"
          onPress={() => tareaDispatch({ tipo: "Agregar", nombre: nuevaTarea })}
        />
      </View>
      <FlatList
        data={tareas}
        renderItem={({ item }) => (
          <Tarea tarea={item} tareaDispatch={tareaDispatch} />
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: Constants.statusBarHeight,
  },
});
