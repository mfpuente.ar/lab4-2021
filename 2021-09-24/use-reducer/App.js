import React, { useReducer, useState } from "react";
import { Button, StyleSheet, Text, View } from "react-native";

function reducer(state, action) {
  switch (action) {
    case "incrementar":
      return { cuenta: state.cuenta + 1 };
    case "decrementar":
      return { cuenta: state.cuenta - 1 };
  }
}

export default function App() {
  const [cuenta, setCuenta] = useState(0);
  const [state, dispatch] = useReducer(reducer, { cuenta: 0 });

  function incrementar() {
    setCuenta((c) => c + 1);
  }

  function decrementar() {
    setCuenta((c) => c - 1);
  }

  return (
    <View style={styles.container}>
      <View style={{ flexDirection: "row" }}>
        <Button title="-" onPress={() => dispatch("decrementar")} />
        <Text>{state.cuenta}</Text>
        <Button title="+" onPress={() => dispatch("incrementar")} />
      </View>
      <View style={{ flexDirection: "row" }}>
        <Button title="-" onPress={() => decrementar()} />
        <Text>{cuenta}</Text>
        <Button title="+" onPress={() => incrementar()} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
