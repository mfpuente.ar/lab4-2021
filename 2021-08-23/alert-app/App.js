import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Button, StyleSheet, Alert, View, Dimensions } from 'react-native';

export default function App() {
  const dimensiones = Dimensions.get('window')
  console.log(dimensiones)
  return (
    <View style={styles.container}>
      <Button title="Alert" onPress={() => {
        Alert.alert('Un titulo', 'mensaje de alerta', [
          { text: 'si', onPress:() => console.log('si')},
          { text: 'no', onPress:() => console.log('no')},
        ])
      }} />
      <Button title="Prompt" onPress={() => {
        //No funciona en Android
        Alert.prompt('titulo', 'Ingrese texto')
      }} />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
