import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={{ backgroundColor: 'red', width: 100, height: 300 }} />
        <View style={{ backgroundColor: 'blue', width: 100, height: 300 }} />
        <View style={{ backgroundColor: 'green', width: 100, height: 300 }} />
        <View style={{ backgroundColor: 'grey', width: 100, height: 300 }} />
        <StatusBar style="auto" />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
});
