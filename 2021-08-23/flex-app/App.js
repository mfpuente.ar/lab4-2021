import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { ImageBackground, SafeAreaView, StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <ImageBackground style={{
        width: 150,
        height: 100,
        justifyContent: 'center',
      }} 
        source ={require('./assets/icon.png')}
        resizeMode="stretch"
      >
        <Text>Hola</Text>
      </ImageBackground>
      <View style={{
        backgroundColor:'green',
        width: 100,
        height: 100
      }}>
        <Text>Texto</Text>
      </View>
      <View style={{
        backgroundColor:'blue',
        width: 100,
        height: 100
      }}>
        <Text>Mundo</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center'
  },
});

